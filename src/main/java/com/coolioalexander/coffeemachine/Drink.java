package com.coolioalexander.coffeemachine;

import java.math.BigDecimal;

public interface Drink {

    String getCode();

    BigDecimal getPrice();

}
