package com.coolioalexander.coffeemachine;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CoffeeMachineTest {

    private Order order;
    private static DrinkMaker drinkMaker;
    private static CashRegister cashRegister;
    private static CoffeeMachine coffeeMachine;

    @BeforeAll
    static void setUp() {
        drinkMaker = mock(DrinkMaker.class);
        cashRegister = mock(CashRegister.class);
        coffeeMachine = new CoffeeMachine(drinkMaker, cashRegister);
    }

    @Test
    void shouldSetDrinkMakerWhenMachineIsSet() {
        assertThat(coffeeMachine.getDrinkMaker()).isEqualTo(drinkMaker);
    }

    @Test
    @DisplayName("Drink maker should receive correct instructions at order")
    void shouldSendCorrectInstructionsToDrinkMaker() {
        ArgumentCaptor<String> instructionsCaptor = ArgumentCaptor.forClass(String.class);
        order = new Order(HotDrink.TEA, 1);
        BigDecimal insertedAmount = new BigDecimal("0.4");

        coffeeMachine.take(order, insertedAmount);

        verify(drinkMaker).process(instructionsCaptor.capture());
        String instructions = instructionsCaptor.getValue();
        assertThat(instructions).isEqualTo("Th:1:0");
    }

    @Test
    void shouldMakeDrinkWhenCorrectAmountIsInserted() {
        order = new Order(HotDrink.COFFEE, 2);
        BigDecimal insertedAmount = new BigDecimal("0.6");

        coffeeMachine.take(order, insertedAmount);

        verify(drinkMaker).process(order.instructions());
    }

    @Test
    @DisplayName("Drink maker should receive missing money message when enough money is not inserted")
    void shouldSendMessageWhenEnoughMoneyIsNotInserted() {
        order = new Order(HotDrink.COFFEE, 2);
        BigDecimal insertedAmount = new BigDecimal("0.4");

        coffeeMachine.take(order, insertedAmount);

        verify(drinkMaker).missMoney("M:0.2");
    }

    @Test
    void shouldThrowExceptionWhenMoneyIsNotInserted() {
        order = new Order(HotDrink.COFFEE, 2);

        Throwable thrown = catchThrowable(() -> coffeeMachine.take(order, null));

        assertThat(thrown)
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Inserted money must not be null");
    }

    @Test
    void shouldRegisterSaleWhenOrderSucceeds() {
        order = new Order(ColdDrink.ORANGE_JUICE);
        BigDecimal insertedAmount = new BigDecimal("0.6");
        doReturn(true).when(drinkMaker).process(any(String.class));

        coffeeMachine.take(order, insertedAmount);

        verify(cashRegister).register(order);
    }

    @Test
    void shouldNotRegisterSaleWhenOrderFails() {
        order = new Order(ColdDrink.ORANGE_JUICE);
        BigDecimal insertedAmount = new BigDecimal("0.6");
        doReturn(false).when(drinkMaker).process(any(String.class));

        coffeeMachine.take(order, insertedAmount);

        verify(cashRegister, never()).register(order);
    }

    @Test
    void shouldPrintReport() {
        coffeeMachine.printReport();

        verify(cashRegister).report();
    }

}
