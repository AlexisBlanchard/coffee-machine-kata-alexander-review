package com.coolioalexander.coffeemachine;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

public class OrderTest {

    private Order order;

    @Test
    void shouldOrderTheaWithOnSugarAndStirrer() {
        order = new Order(HotDrink.TEA, 1);

        String instructions = order.instructions();

        assertThat("Th:1:0").isEqualTo(instructions);
    }

    @Test
    void shouldOrderChocolateWithoutSugarAndNoStirrer() {
        order = new Order(HotDrink.CHOCOLATE);

        String instructions = order.instructions();

        assertThat("Hh::").isEqualTo(instructions);
    }

    @Test
    void shouldOrderCoffeeWithTwoSugarAndStirrer() {
        order = new Order(HotDrink.COFFEE, 2);

        String instructions = order.instructions();

        assertThat("Ch:2:0").contains(instructions);
    }

    @Test
    void shouldOrderOrangeWithoutSugarAndNoStirrer() {
        order = new Order(ColdDrink.ORANGE_JUICE, 0);

        String instructions = order.instructions();

        assertThat("O::").contains(instructions);
    }

    @Test
    void shouldThrowExceptionWhenDrinkIsNull() {
        Throwable thrown = catchThrowable(() -> new Order(null));

        assertThat(thrown)
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("Drink must not be null");
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 3})
    void shouldThrowExceptionWhenSugarQuantityIsNotRequired(int sugarQuantity) {
        Throwable thrown = catchThrowable(() -> new Order(HotDrink.TEA, sugarQuantity));

        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Quantity must be between 0 and 2");
    }

}
